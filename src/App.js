import React from 'react'
import './styles/_main.scss'
import Navbar from './components/navbar'
import Home from './components/home/home'
import Cards from './components/cards/cards'
import Services from './components/services/services'
import Count from './components/count/count'
import Process from './components/process/process'
import Team from './components/team/team'
import News from './components/news/news'
import Fotter from './components/fotter/fotter'

function App () {
  return (
    <div className="root">
      <Navbar/>
      <Home/>
      <Cards/>
      <div className='container'>
      <Services/>
      <Count/>
      <Process/>
      </div>
      <Team/>
      <News/>
      <Fotter/>
    </div>
  )
}

export default App
