import React from 'react'
import { BiCast } from 'react-icons/bi'
import { FiLayers } from 'react-icons/fi'
import { BsPeople } from 'react-icons/bs'

function Cards () {
  return (
    <div className='card_container'>
          <div className='card_container_content'>
      <div className='card-icon'><BiCast/></div>
        <h3>Business Strategy</h3>
        <p>I throw my self down among the tall grass by the stream as i lie clos the earth</p>
     </div>

     <div className='card_container_content'>
      <div className='card-icon'><FiLayers/></div>
        <h3>Website Development</h3>
        <p>I throw my self down among the tall grass by the stream as i lie clos the earth</p>
     </div>

     <div className='card_container_content'>
      <div className='card-icon'><BsPeople/></div>
        <h3>Marketing & Services</h3>
        <p>I throw my self down among the tall grass by the stream as i lie clos the earth</p>
     </div>
    </div>
  )
}

export default Cards
