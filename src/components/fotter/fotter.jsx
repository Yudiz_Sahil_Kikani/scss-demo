import React from 'react'
import { CiFacebook, CiTwitter } from 'react-icons/ci'
import { AiOutlineInstagram, AiOutlineSkype } from 'react-icons/ai'

function Fotter () {
  return (
    <>
    <div className='fotter'>
        <p>Redy to do this</p>
        <h1>Lets get to work</h1>
        <button>Contact us</button>
    </div>
    <div className='fotter-links'>
        <a><CiFacebook/></a>
        <a><AiOutlineInstagram/></a>
        <a><AiOutlineSkype/></a>
        <a><CiTwitter/></a>
    </div>
    </>
  )
}

export default Fotter
