import { React, useState } from 'react'
import logo from '../assets/img/logo.png'
import { AiOutlineMenu } from 'react-icons/ai'

function Navbar () {
  const [show, setShow] = useState(false)
  return (
    <div className='navbar_container'>
    <div className='navbar_container_logo'>
        <img src={logo}/>
    </div>
    <div className='navbar_container_bars' onClick={(a) => setShow(true)}><AiOutlineMenu/></div>
    {
      show
        ? <div className='toggle_navlinks' onClick={(a) => setShow(false)}>
          <a href='home'>Home</a>
        <a href='services'>Service</a>
        <a href='about'>About</a>
        <a href='pages'>Pages</a>
        <a href='blocks'>Blocks</a>
        <button>Buy Now</button>
      </div>
        : null
    }
    <div className='navbar_container_links'>
        <a href='home'>Home</a>
        <a href='services'>Service</a>
        <a href='about'>About</a>
        <a href='pages'>Pages</a>
        <a href='blocks'>Blocks</a>
        <button>Buy Now</button>
    </div>
    </div>
  )
}

export default Navbar
