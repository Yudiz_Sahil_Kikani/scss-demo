import React from 'react'
import tm from '../../assets/img/profile.jpg'

function Process () {
  return (
    <div className='process'>
        <div className='bd'>
        <img src={tm}/>
      </div>
      <div className='process-content'>
        <h1 className='process-name'>Working Process</h1>
        <p>
        A work process is any method that employees use to create value for the company. Companies often create company or industry-specific work processes to accomplish tasks. For example, a manufacturing company might create a specific work process for producing its clothing line, while a marketing company might create a work process for establishing a new social media ad campaign. You can include almost anything employees do in the definition of a work process.
        </p>
        <button className='process-btn'>See How It Works</button>
      </div>
        </div>
  )
}

export default Process
