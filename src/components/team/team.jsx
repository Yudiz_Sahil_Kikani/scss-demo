import React from 'react'
import bd from '../../assets/img/bd.jpg'
import bd1 from '../../assets/img/bd1.jpg'
import bd2 from '../../assets/img/bd2.jpg'

// import { AiFillFacebook, AiFillLinkedin, AiFillTwitterCircle } from 'react-icons/ai'
import { FaFacebookF, FaLinkedinIn, FaTwitter } from 'react-icons/fa'

function Team () {
  return (
    <>
    <div className='team-title'>
    <h1>Managing Team</h1>
    <p>A work process is any method that employees use to create value for the company. Companies often create company or industry-specific work processes to accomplish tasks</p>
    </div>
    <div className='team'>
        <div className='team-member'>
        <img className='member-pic' src={bd}/>
        <h4>Jone Jan</h4>
        <div className='icon'><span><FaFacebookF/></span><span><FaLinkedinIn/></span><span><FaTwitter/></span></div>
      </div>

      <div className='team-member'>
        <img className='member-pic' src={bd1}/>
        <h4>Jone Jan</h4>
        <div className='icon'><span><FaFacebookF/></span><span><FaLinkedinIn/></span><span><FaTwitter/></span></div>
      </div>

      <div className='team-member'>
        <img className='member-pic' src={bd2}/>
        <h4>Jone Jan</h4>
        <div className='icon'><span><FaFacebookF/></span><span><FaLinkedinIn/></span><span><FaTwitter/></span></div>
      </div>

      <div className='team-member'>
        <img className='member-pic' src={bd}/>
        <h4>Jone Jan</h4>
        <div className='icon'><span><FaFacebookF/></span><span><FaLinkedinIn/></span><span><FaTwitter/></span></div>
      </div>
    </div>
    </>
  )
}

export default Team
